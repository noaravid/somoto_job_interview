import {Component, OnInit} from '@angular/core';
import {DataService} from './services/data.service';
import {CardModel} from './models/card.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  opened: boolean = false;
  title = 'somoto-job-interview';
  cardsDataForNavBar: CardModel[];
  route: string;


  constructor(private dataService: DataService) {}

  ngOnInit() {
    this.cardsDataForNavBar = this.dataService.getData();
  }


}
