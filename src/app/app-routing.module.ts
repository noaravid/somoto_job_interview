import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {CardsComponent} from './cards/cards.component';
import {CardDetailComponent} from './card-detail/card-detail.component';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  {path: 'home', component: CardsComponent},
  {path: 'details/:index', component: CardDetailComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
