import {Component, OnInit} from '@angular/core';
import {CardModel} from '../models/card.model';
import {ActivatedRoute} from '@angular/router';
import {DataService} from '../services/data.service';

@Component({
  selector: 'app-card-detail',
  templateUrl: './card-detail.component.html',
  styleUrls: ['./card-detail.component.css']
})
export class CardDetailComponent implements OnInit {
  cardData: CardModel;

  constructor(private activatedRoute: ActivatedRoute,
              private dataService: DataService) { }

  ngOnInit() {
    const index = this.activatedRoute.snapshot.params['index'];
    this.cardData = this.dataService.getCard(index);
  }


}
