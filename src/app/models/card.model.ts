export class CardModel {
  title: string;
  article?: string;
  image?: string;
}
