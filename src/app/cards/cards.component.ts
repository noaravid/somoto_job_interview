import { Component, OnInit } from '@angular/core';
import {DataService} from '../services/data.service';
import {CardModel} from '../models/card.model';

@Component({
  selector: 'app-cards',
  templateUrl: './cards.component.html',
  styleUrls: ['./cards.component.css']
})
export class CardsComponent implements OnInit {
  cardsData: CardModel[];

  constructor(private dataService: DataService) { }

  ngOnInit() {
    this.cardsData = this.dataService.getData();
  }

}
